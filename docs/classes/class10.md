### classes[9] = "Phylogenetics Pt. V - Bayesian Inference"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">Bayesian Inference</li>
<li class="fragment">MCMCMC based tree construction</li>
<li class="fragment">Practical work</li>
</ul>

---

### Phylogenetic reconstruction methods

<ul>
<li class="fragment" data-fragment-index="1">There are several methods for phylogenetic reconstruction</li>
<li class="fragment" data-fragment-index="2">Those that use character states</li>
  <ul>
  <li class="fragment" data-fragment-index="3">Maximum parsimony</li>
  <li class="fragment" data-fragment-index="4">Maximum likelihood</li>
  <li class="fragment" data-fragment-index="5"><span class="fragment highlight-red" data-fragment-index="9">Bayesian inference</span></li>
  </ul>
<li class="fragment" data-fragment-index="6">Those that use distance measurements</li>
  <ul>
  <li class="fragment" data-fragment-index="7">UPGMA</li>
  <li class="fragment" data-fragment-index="8">Neighbour joining</li>
  </ul>
</ul>

---

### How does Bayesian inference work?

<img src="C10_assets/bayes.gif" style="background:none; border:none; box-shadow:none; float:right">

<ul>
<li class="fragment">Consider H<sub>1</sub> and H<sub>2</sub></li>
<li class="fragment">It has the following Likelihood ratio:</li>
  <ul>
  <li class="fragment">
<math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
 <semantics>
  <mrow>
   <mfrac>
    <mrow>
     <mi mathvariant="italic">Prob</mi>
     <mrow>
      <mo fence="true" stretchy="false">(</mo>
      <mrow>
       <mrow>
        <mi>D</mi>
        <mo stretchy="false">∣</mo>
        <msub>
         <mi>H</mi>
         <mn>1</mn>
        </msub>
       </mrow>
      </mrow>
      <mo fence="true" stretchy="false">)</mo>
     </mrow>
    </mrow>
    <mrow>
     <mi mathvariant="italic">Prob</mi>
     <mrow>
      <mo fence="true" stretchy="false">(</mo>
      <mrow>
       <mrow>
        <mi>D</mi>
        <mo stretchy="false">∣</mo>
        <msub>
         <mi>H</mi>
         <mn>2</mn>
        </msub>
       </mrow>
      </mrow>
      <mo fence="true" stretchy="false">)</mo>
     </mrow>
    </mrow>
   </mfrac>
   <mo stretchy="false">=</mo>
   <mrow>
    <mn>1</mn>
    <mo stretchy="false">/</mo>
    <mn>2</mn>
   </mrow>
  </mrow>
  <annotation encoding="StarMath 5.0">{Prob( D divides H_{1} )} over {Prob( D divides H_{2} )} = 1 / 2</annotation>
 </semantics>
</math>
  </li>
  <li class="fragment">These results favour H<sub>2</sub> at a 1:2 ratio</li>
  </ul>
<li class="fragment">Let's assume a *Prior* of 3/2:</li>
  <ul>
  <li class="fragment">Reclaculate the ratio:</li>
  <li class="fragment">(1/2)\*(3/2) = 3/4</li>
  <li class="fragment">Results now favour H<sub>2</sub>, but at a 3:4 ratio</li>
  <li class="fragment">3/4 is the *Posterior probability*</li>
  </ul>
</ul>


|||

### The Bayes Theorem

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
 <semantics>
  <mrow>
   <mi mathvariant="italic">Prob</mi>
   <mrow>
    <mrow>
     <mo fence="true" stretchy="false">(</mo>
     <mrow>
      <mrow>
       <mi>H</mi>
       <mo stretchy="false">∣</mo>
       <mi>D</mi>
      </mrow>
     </mrow>
     <mo fence="true" stretchy="false">)</mo>
    </mrow>
    <mo stretchy="false">=</mo>
    <mfrac>
     <mrow>
      <mi mathvariant="italic">Prob</mi>
      <mrow>
       <mo fence="true" stretchy="false">(</mo>
       <mrow>
        <mi>H</mi>
       </mrow>
       <mo fence="true" stretchy="false">)</mo>
      </mrow>
      <mi mathvariant="italic">Prob</mi>
      <mrow>
       <mo fence="true" stretchy="false">(</mo>
       <mrow>
        <mrow>
         <mi>D</mi>
         <mo stretchy="false">∣</mo>
         <mi>H</mi>
        </mrow>
       </mrow>
       <mo fence="true" stretchy="false">)</mo>
      </mrow>
     </mrow>
     <mrow>
      <mrow>
       <msub>
        <mo stretchy="false">∑</mo>
        <mi>H</mi>
       </msub>
       <mi mathvariant="italic">Prob</mi>
      </mrow>
      <mrow>
       <mo fence="true" stretchy="false">(</mo>
       <mrow>
        <mi>H</mi>
       </mrow>
       <mo fence="true" stretchy="false">)</mo>
      </mrow>
      <mi mathvariant="italic">Prob</mi>
      <mrow>
       <mo fence="true" stretchy="false">(</mo>
       <mrow>
        <mrow>
         <mi>D</mi>
         <mo stretchy="false">∣</mo>
         <mi>H</mi>
        </mrow>
       </mrow>
       <mo fence="true" stretchy="false">)</mo>
      </mrow>
     </mrow>
    </mfrac>
   </mrow>
  </mrow>
  <annotation encoding="StarMath 5.0">Prob (H divides D) = {Prob (H) Prob( D divides H )} over {SUM_H Prob(H) Prob( D divides H )}</annotation>
 </semantics>
</math>

---

### The effect of Priors

<img src="C10_assets/priors.png" style="background:none; border:none; box-shadow:none;">

|||

### Intromission

Let's do this the hard way

([Here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C10_assets/Prior_effects.R) is the code for the plots in the presentation)

---

### Tree building

<ul>
<li class="fragment">BI finds the best tree using an MCMCMC approach</li>
  <ul>
  <li class="fragment">**M**etropolis **C**oupled **M**arkov **C**hain **M**onte **C**arlo</li>
  <li class="fragment">Iterative approach</li>
  </ul>
</ul>

<img src="C10_assets/monte_carlo.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Markov Chain Monte Carlo

<ol>
<li class="fragment">Start with a random tree *T<sub>i</sub>*</li>
<li class="fragment">Pick and propose a neighbour tree *T<sub>j</sub>*</li>
<li class="fragment">Compute the trees' probability density functions:</li>
  <ul style="list-style-type:none;">
  <li class="fragment">
<math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
 <semantics>
  <mrow>
   <mi>R</mi>
   <mo stretchy="false">=</mo>
   <mfrac>
    <mrow>
     <mi>f</mi>
     <mrow>
      <mo fence="true" stretchy="false">(</mo>
      <mrow>
       <msub>
        <mi>T</mi>
        <mi>j</mi>
       </msub>
      </mrow>
      <mo fence="true" stretchy="false">)</mo>
     </mrow>
    </mrow>
    <mrow>
     <mi>f</mi>
     <mrow>
      <mo fence="true" stretchy="false">(</mo>
      <mrow>
       <msub>
        <mi>T</mi>
        <mi>i</mi>
       </msub>
      </mrow>
      <mo fence="true" stretchy="false">)</mo>
     </mrow>
    </mrow>
   </mfrac>
  </mrow>
  <annotation encoding="StarMath 5.0">R = {f( T_j )} over {f( T_i )}</annotation>
 </semantics>
</math></li>
  </ul>
<li class="fragment">If *R >= 1*, accept the new tree</li>
<li class="fragment">Else, draw a random number "*W*" from a uniform distribution</li>
  <ul style="list-style-type:none;">
  <li class="fragment">5.1 If *W < R*, accept the new tree</li>
  <li class="fragment">5.2 Else, keep the old tree</li>
  </ul>
<li class="fragment">Return to step 2</li>
</ol>

|||

### Markov Chain Monte Carlo

<img src="C10_assets/MCMCMC.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Markov Chain Monte Carlo

<div style="position:relative; width:800px; height:600px; margin:0 auto;">
  <img class="fragment fade-out" data-fragment-index="0" width="800" height="600" src="C10_assets/likelihoods.png" style="position:absolute;top:0;left:0;background:none;border:none;box-shadow:none;" />
    <img class="fragment fade-in" data-fragment-index="0" width="800" height="600" src="C10_assets/likelihoods_2.png" style="position:absolute;top:0;left:0;background:none;border:none;box-shadow:none;" />
    </div>

---

### Evolutionary models in Bayesian inference

<ul>
<li class="fragment">The most used implementation of this method is `MrBayes`</li>
<li class="fragment">`MrBayes` can use evolutionary models by setting 2 parameters:</li>
  <ul>
  <li class="fragment">`nst=X` (sets the number of possible substitutions)</li>
  <li class="fragment">`prset=XXX` (sets the prior distribution and nucleotide rates)</li>
  </ul>
<li class="fragment">Understanding and modifying these is beyond the scope of this course</li>
</ul>

---

### Practical work

* Install the software [mrbayes](http://nbisweden.github.io/MrBayes/)
* Build a Bayesian Inference tree on each of the 3 gene fragments used in Akihito *et al.* 2016
* Interpret the trees (consult the original paper if you have to)
* Do not forget to look at the Posterior probability values!
* You can find a copy of the data [here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C08_assets/Akihito_data.tar.xz) in case you have lost yours for some reason
* Use the program `figtree` to view and manipulate the resulting trees
* *Hint*: You can include MrBayes commands in the NEXUS file. [Here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C10_assets/12sV2.nex) is an example for you to adapt.

---

### References

* [Count Bayesie](https://www.countbayesie.com/blog/2015/2/18/hans-solo-and-bayesian-priors)
* [MrBayes manual](http://mrbayes.sourceforge.net/wiki/index.php/Manual_3.2)
* [Evolutionary models in MrBayes](http://mrbayes.sourceforge.net/wiki/index.php/Tutorial_3.2#Specifying_a_Model)

