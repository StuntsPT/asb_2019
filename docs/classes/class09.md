### classes[8] = "Phylogenetics Pt. IV - Evolutionary models + ML"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">Evolutionary models</li>
<li class="fragment">Maximum likelihood</li>
<li class="fragment">Practical work</li>
</ul>

---

### Phylogenetic reconstruction methods

<ul>
<li class="fragment" data-fragment-index="1">There are several methods for phylogenetic reconstruction</li>
<li class="fragment" data-fragment-index="2">Those that use character states</li>
  <ul>
  <li class="fragment" data-fragment-index="3">Maximum parsimony</li>
  <li class="fragment" data-fragment-index="4"><span class="fragment highlight-red" data-fragment-index="9">Maximum likelihood</span></li>
  <li class="fragment" data-fragment-index="5">Bayesian inference</li>
  </ul>
<li class="fragment" data-fragment-index="6">Those that use distance measurements</li>
  <ul>
  <li class="fragment" data-fragment-index="7">UPGMA</li>
  <li class="fragment" data-fragment-index="8">Neighbour joining</li>
  </ul>
</ul>

---

### A problem with Parsimony

<img src="C09_assets/01saturation.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">Sequence saturation</li>
</ul>

|||

### Sequence evolution and mutation accumulation

<img src="C09_assets/02differentiation.png" style="background:none; border:none; box-shadow:none;">

|||

### Sequence evolution and mutation accumulation

<img src="C09_assets/03differentiation_bases.png" style="background:none; border:none; box-shadow:none;">

|||

### As time goes by...

<img src="C09_assets/04time_goes_by.png" style="background:none; border:none; box-shadow:none;">

|||

### However, thing are not always so simple

<img src="C09_assets/05two_subs_one_diff.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">2 substitutions, 1 difference</li>
</ul>

|||

### In many possible ways

<img src="C09_assets/06two_subs_one_diff.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">2 subs., 1 diff.</li>
</ul>

|||

### In many possible ways

<img src="C09_assets/07two_subs_zero_diff.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">2 subs., 0 diff.</li>
</ul>

|||

### In many possible ways

<img src="C09_assets/08three_subs_zero_diff.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">3 subs., 0 diff.</li>
</ul>

|||

### In many possible ways

<img src="C09_assets/09two_subs_zero_diff.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">2 subs., 0 diff.</li>
</ul>

|||

### Which takes us back to the start!

<img src="C09_assets/01saturation.png" style="background:none; border:none; box-shadow:none;">

---

### Evolutionary models

<ul>
<li class="fragment">Some models were thus devised to overcome the saturation issue</li>
<li class="fragment">They are based on the *probability* of a determined change occurring over time</li>
  <ul>
  <li class="fragment">*What is the probability of an A mutating into a T?*</li>
  </ul>
</ul>

<img src="C09_assets/Transitions_vs_transversions.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Substitution probability matrix

<img src="C09_assets/Sub_prob_matrix.png" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">The probability of an *A* mutating into a *T* is given by *p*<sub>AT</sub></li>
</ul>

|||

### And finally...

<ul>
<li class="fragment">The probability matrix is then used to derive a *Maximum Likelihood* functions that recalculates the difference between sequences</li>
<li class="fragment">Mathematical demonstration in 17 simple steps:</li>
</ul>

<img src="C09_assets/no_way.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### What about base frequencies?

<img src="C09_assets/gamma.png" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Model assumptions

<img src="C09_assets/Assumptions.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

<ul>
<li class="fragment">Constant substitution rate over time and across lineages</li>
<li class="fragment">Sites mutate independently</li>
<li class="fragment">Probability of each position mutating is identical and constant across time</li>
</ul>

---

### Choosing models

<ul>
<li class="fragment">Many models were devised, throughout time, in increasing order of complexity</li>
  <ul>
  <li class="fragment">[Jukes-Cantor](http://treethinkers.org/jukes-cantor-model-of-dna-substitution/)</li>
  <li class="fragment">[Kimura-2-Parameter](https://www.megasoftware.net/mega4/WebHelp/part_iv___evolutionary_analysis/computing_evolutionary_distances/distance_models/nucleotide_substitution_models/hc_kimura_2_parameter_distance.htm)</li>
  <li class="fragment">[Hasegawa, Kishino & Yano](https://www.ncbi.nlm.nih.gov/pubmed/3934395)</li>
  <li class="fragment">[GTR](https://en.wikipedia.org/wiki/Substitution_model#GTR:_Generalised_time_reversible)</li>
  </ul>
<li class="fragment">There is software to test and choose the best model to fit the data</li>
<li class="fragment">Currently, with RAxML there is no need</li>
</ul>

<img src="C09_assets/GTR.png" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Maximum Likelihood

<ul>
<li class="fragment">Best explanation for observed outcome</li>
<li class="fragment">Requires:</li>
  <ul>
  <li class="fragment">Sequence data</li>
  <li class="fragment">Evolutionary model</li>
  <li class="fragment">Tree (topology and branch lengths)</li>
  </ul>
<li class="fragment">For any given topology, what set of branch lengths makes the observed data most likely?</li>
<li class="fragment">Which of all possible trees has the greatest likelihood?</li>
</ul>

<a href="https://xkcd.com/1831/"><img src="C09_assets/here_to_help.png" style="background:none; border:none; box-shadow:none;" class="fragment"></a>

|||

### Maximum Likelihood

<img src="C09_assets/ML_trees.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Maximum Likelihood

<ul>
<li class="fragment">Assert the probability of each possible combination on each site</li>
<li class="fragment">...for every site</li>
  <ul>
  <li class="fragment">Then sum the partial likelihoods to get a total likelihood</li>
  </ul>
<li class="fragment">...for every tree</li>
  <ul>
  <li class="fragment">And the best likelihood is chosen as the best tree</li>
  </ul>
</ul>

---

### Tree branch support

<ul>
<li class="fragment">Have you noticed a number above each branch on trees?</li>
<li class="fragment">This number indicates how well supported each branch is</li>
  <ul>
  <li class="fragment">Arbitrary value</li>
  <li class="fragment">Represents *precision*, not accuracy</li>
  </ul>
<li class="fragment">How are these obtained?</li>
</ul>


|||

### Tree branch support

<img src="C09_assets/bootstrap.png" style="background:none; border:none; box-shadow:none;">

---

### Practical work

* Install the software [raxml](https://cme.h-its.org/exelixis/web/software/raxml/index.html)
* Build a Maximum Likelihood tree on each of the 3 gene fragments used in Akihito *et al.* 2016
* Interpret the trees (consult the original paper if you have to)
* Obtain the bootstrap values
* You can find a copy of the data [here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C08_assets/Akihito_data.tar.xz) in case you have lost yours for some reason
* *Hint*: You can get RAxML quickly running using this command as a base:

```bash
raxmlHPC-AVX2 -f a -m GTRCAT -p 112358 -x 112358 -N 100 -s /path/to/your/aligned/file.fasta -n my_run01
```

---

### References

* [Sequence saturation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3057953/)
* [Evolutionary models](https://en.wikipedia.org/wiki/Models_of_DNA_evolution)
* [Maximum Likelihood inference](https://scholarship.claremont.edu/cgi/viewcontent.cgi?article=1047&context=scripps_theses) (especially the introduction)
* [RAxML docs](https://cme.h-its.org/exelixis/resource/download/NewManual.pdf)

