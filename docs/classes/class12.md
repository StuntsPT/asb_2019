### Classes[11] = "High Throughput Sequencing"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">Sequence representation reminder</li>
<li class="fragment">Sanger sequence data</li>
<li class="fragment">High Throughput data</li>
  <ul>
  <li class="fragment">Technologies</li>
<li class="fragment">Assemblies</li>
  </ul>
<li class="fragment">Practical examples</li>
</ul>

---

### Sanger sequencing

<ul>
<li class="fragment">Primer based</li>
<li class="fragment">Up to 1Kbp reads</li>
<li class="fragment">Large files</li>
<li class="fragment">"Low" throughput</li>
  <ul>
  <li class="fragment">~40 min per run</li>
  <li class="fragment">96 plex</li>
  <li class="fragment">144Kbp per hour</li>
  <li class="fragment">3.5Mbp per day</li>
  </ul>
</ul>

<img src="C12_assets/chroma.png" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### High throughput sequencing

<ul>
<li class="fragment">What if we need to scale things?</li>
  <ul>
  <li class="fragment">(And not spend our entire budget on sequencing)</li>
  </ul>
<li class="fragment">Unknown genome regions</li>
  <ul>
  <li class="fragment">Non-model organisms</li>
  </ul>
<li class="fragment">We need more power</li>
</ul>

<a href="https://what-if.xkcd.com/13/"><img class="fragment" src="C12_assets/laser_pointer_more_power.png" style="background:none; border:none; box-shadow:none;"></a>

---

### Scaling things up

![Sequencing techs](C12_assets/Seq_techs.jpg)

---

### Amplification based tech

<img src="C12_assets/HTS_Amplifications.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Single molecule tech

<img src="C12_assets/HTS_Single.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Scaling things up

![Sequencing techs](C12_assets/Seq_techs.jpg)

|||

### No more chromatograms for you!

<ul>
<li class="fragment">Sequences of 1Kbp</li>
<li class="fragment">1000 reads provide ~1Mbp</li>
<li class="fragment">Each chromatogram takes 100 ~ 200 KB</li>
  <ul>
  <li class="fragment">1000 reads (1Mbp) * 150KB = 150MB</li>
  <li class="fragment">100000 reads (100Mbp) * 150KB = 15GB</li>
  </ul>
</ul>

<img src="C12_assets/Disk_full.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Enter the [FASTQ](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/FileFormat_FASTQ-files_swBS.htm) [format](https://pythonhosted.org/OBITools/fastq.html)

<ul>
<li class="fragment">Like a FASTA file, but with quality scores per base</li>
<li class="fragment">Each sequence is composed of 4 lines:</li>
</ul>

<pre class="fragment"><code>@Sequence_identifier
ATGCGATAGCTGACTGACTAGCT
+(Seq_ID again)
!''*(((((******+***,-</code></pre>

<img class="fragment" src="C12_assets/Probabilitymetrics.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### FASTQ format

```bash
# Obtain a fastq sequence file
cd ~/
mkdir hts_data
cd hts_data
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C12_assets/short_reads.fastq

# Look at the file contents
nano short_reads.fastq

# How much space does this file occupy?
# How many sequences are represented?
# How many base pairs do you estimate? (you may want to answer this later)

```

---

### Quality Control

<ul>
<li class="fragment">QC is performed massively</li>
<li class="fragment">[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)</li>
</ul>

<img class="fragment" src="C12_assets/Probabilitymetrics.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### Quality Control

```bash
conda install -c bioconda fastqc

fastqc

fastqc /path/to/your/file.fastq

```

---

### Assemblies

<ul>
<li class="fragment">Sequence assemblies are a huge problem</li>
  <ul>
  <li class="fragment">HTS reads come from random genome locations</li>
  <li class="fragment">We could use an entire semester to deal with this problem</li>
  </ul>
<li class="fragment">Two types of sequence assembly</li>
  <ul>
  <li class="fragment">*Mapping* assemblies (reference available)</li>
  <li class="fragment">*Denovo* assemblies (reference unavailable)</li>
  </ul>
</ul>

|||

### Assemblies

<img src="C12_assets/NGS-image.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="C12_assets/Puzzle_and_box.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="C12_assets/Puzzle_no_box.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="C12_assets/krypt-puzzle-image.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="C12_assets/EST_puzzle.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="C12_assets/mismatch.jpg" style="background:none; border:none; box-shadow:none;"></a>

---

### A reference sequence is available

<ul>
<li class="fragment">A reference sequence solves most assembling issues</li>
  <ul>
  <li class="fragment">We just need to align our reads to the reference "backbone"</li>
  <li class="fragment">There are plenty of algorithms for this</li>
    <ul>
    <li class="fragment">Bowtie2</li>
    <li class="fragment">BWA</li>
    </ul>
  </ul>
</ul>

|||

### Map it yourself!

```bash
# Install the required tools:
conda install -c bioconda bowtie2 samtools tablet

# Get a reference sequence
cd ~/hts_data
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C12_assets/reference.fasta

# Now, let the magic begin.
# First we need to create an index from our reference sequence and look at what happened
bowtie2-build reference.fasta reference
ls

# Next, we align our reads to our reference and look at the resulting file
bowtie2 -x reference -U short_reads.fastq -S assembly.sam
nano assembly.sam

# Then we convert the assembly to a sorted binary file (BAM) using samtools
samtools view -bS assembly.sam | samtools sort -o assembly.bam

# Finally we index our BAM file
# Don't forget to look at the file sizes
samtools index assembly.bam
ls -l

# Now we can look at the final result using tablet
# "Open Assembly" -> Select the BAM file as the assembly and the fasta file as the reference
tablet

```

|||

### [SAM/BAM format](https://samtools.github.io/hts-specs/SAMv1.pdf)

<ul>
<li class="fragment">The "standard" way to represent assembled data</li>
  <ul>
  <li class="fragment">Contain the reads and their coordinates relative to a reference / each other</li>
  <li class="fragment">A BAM file is a binary version of a SAM file</li>
  <li class="fragment">BAM files can be indexed</li>
  </ul>
</ul>

---

### Fun with assemblies!

<ul>
<li class="fragment">Once we have a SAM/BAM file there is a lot we can do</li>
  <ul>
  <li class="fragment">Get coverage data (AKA depth)</li>
  <li class="fragment">Call variants</li>
  </ul>
</ul>

<img class="fragment" src="C12_assets/fun.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Fun with assemblies!

```bash
conda install -c bioconda bcftools

samtools mpileup -uf reference.fasta assembly.bam | bcftools call -m -v - > Variants.vcf
# This command will output a new file, where all variant positions are logged

```

|||

### More on VCF?

<ul>
<li class="fragment">[**V**ariant **C**alling **F**ormat](https://samtools.github.io/hts-specs/VCFv4.2.pdf)</li>
  <ul>
  <li class="fragment">Contains data on variants</li>
  <li class="fragment">Very compact format</li>
  </ul>
</ul>

---

### What if there is no reference?

<ul>
<li class="fragment">The problem is now **a lot** harder</li>
  <ul>
  <li class="fragment">We need to align our reads to each other</li>
  <li class="fragment">There are also plenty of algorithms for this</li>
    <ul>
    <li class="fragment">Spades</li>
    <li class="fragment">Trinity</li>
    <li class="fragment">...many more!</li>
    </ul>
  </ul>
</ul>

<img src="C12_assets/Not_today.png" style="background:none; border:none; box-shadow:none;" class="fragment"></a>

---

### Obtaining HTS data

<ul>
<li class="fragment">Meet the [SRA database](https://www.ncbi.nlm.nih.gov/sra)</li>
  <ul>
  <li class="fragment">**S**equence **R**ead **A**rchive</li>
  <li class="fragment">Contains *raw* sequence data</li>
  <li class="fragment">Search for "phage"</li>
  <li class="fragment">There is something missing though...</li>
  </ul>
</ul>

<img src="C12_assets/data_center.jpg" style="background:none; border:none; box-shadow:none;" class="fragment"></a>

|||

### SRA tools

<ul>
<li class="fragment">In order to get data from SRA you need a [toolkit](https://github.com/ncbi/sra-tools)</li>
  <ul>
  <li class="fragment">`sra-tools`</li>
  <li class="fragment">Contains a lot of tools to handle SRA data</li>
  <li class="fragment">For now we will focus on `fasterq-dump`</li>
  </ul>
</ul>

<img src="C12_assets/tools.jpg" style="background:none; border:none; box-shadow:none;" class="fragment"></a>

|||

### Getting data

```bash
conda install -c bioconda sra-tools

mkdir sra_seqs
cd sra_seqs
fasterq-dump "ACCESSION"  # Accession is also called "Run"

```

---

### Practical work

* Obtain HTS data from SRA for a phage of your choice
* Obtain the genome of this phage from NCBI genomes database
* Map your HTS data to the reference genome
* Obtain the average coverage of your genome
* Find which position has the highest coverage
* Obtain the average coverage of the 200 base pairs around the higest coverage position
* Perform a basic SNP calling on your assembled genome (between your HTS data and the reference sequence)

---

### References

* [High Throughput Sequencing techs](https://dx.doi.org/10.1016%2Fj.molcel.2015.05.004)
* [Bowtie2 Tutorial](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#getting-started-with-bowtie-2-lambda-phage-example)
* [BWA](http://bio-bwa.sourceforge.net/)
* [SamTools Documentation](http://www.htslib.org/doc/)
* [**V**ariant **C**alling **F**ormat](https://samtools.github.io/hts-specs/VCFv4.2.pdf)
* [Variant calling](https://wikis.utexas.edu/display/bioiteam/Variant+calling+using+SAMtools)
* [Spades](http://cab.spbu.ru/software/spades/)
* [SRA Documentation](https://www.ncbi.nlm.nih.gov/books/NBK158899/)
