### Classes[4] = "A Primer on Biological Seqs"

---

### Summary

<ul>
<li class="fragment">Sequence representation reminder</li>
<li class="fragment">"Low Throughput" Sequencing</li>
<li class="fragment">Getting data Pt.1</li>
<li class="fragment">File formats</li>
<li class="fragment">Sequence databases</li>
<li class="fragment">Getting data Pt.2</li>
<li class="fragment">Practical examples</li>
</ul>

---

### Short reminder

![IUPAC](C05_assets/IUPAC.png)

IUPAC codes

|||

## Nucleotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>. or -</td>
<td>gap</td>
</tr>
</table>

---

### Getting data

![DNA lab](C05_assets/DNA_lab.jpg)

---

### Sanger sequencing

* [How Sanger sequencing works](https://www.thermofisher.com/us/en/home/life-science/sequencing/sanger-sequencing/sanger-sequencing-workflow.html)

<img src="C05_assets/chroma.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Sanger sequencing

<ul>
<li class="fragment">Golden Standard</li>
<li class="fragment">High quality</li>
<li class="fragment">Low throughput</li>
<li class="fragment">Large files</li>
  <ul>
  <li class="fragment">[Here are example trace files](C05_assets/Trace_files.zip)</li>
  <li class="fragment">How do we read these?</li>
    <ul>
    <li class="fragment">[CutePeaks](https://github.com/labsquare/CutePeaks)</li>
  <li class="fragment">`ATCGTGACGATGCTGAGATCGCTAGCTAAACTCGATACGCTACTACGA`</li>
  </ul>
</ul>

|||

### Sanger sequencing

Get CutePeaks up and running, and use it to look at a chromatogram.

---

### Analysing trace files

<ul>
<li class="fragment">Today we will use [SeqTrace](https://github.com/stuckyb/seqtrace)</li>
<li class="fragment">Your VM needs some dependecies, though</li>
  <ul>
  <li class="fragment">`python2`</li>
  <li class="fragment">`python-gtk2`</li>
  </ul>
<li class="fragment">We will make [these trace files](C05_assets/New_traces.zip) ready for "production"</li>
</ul>

---

### "Low Throughput" Sequence formats

<ul>
  <li class="fragment">Some of the most frequent file formats</li>
  <li class="fragment">[FASTA](https://zhanglab.ccmb.med.umich.edu/FASTA/)</li>
  <li class="fragment">[GB](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html)</li>
  <li class="fragment">[MEGA](https://www.megasoftware.net/webhelp/walk_through_mega/mega_basics_hc.htm)</li>
  <li class="fragment">[ALN](http://meme-suite.org/doc/clustalw-format.html)</li>
  <li class="fragment">[NEXUS](http://hydrodictyon.eeb.uconn.edu/eebedia/index.php/Phylogenetics:_NEXUS_Format)</li>
  <li class="fragment">[PHYLIP](http://www.phylo.org/tools/obsolete/phylip.html)</li>
  <li class="fragment">...</li>
</ul>

|||

### "Low Throughput" Sequence formats

* [Sequence storage files](C05_assets/unaligned_seqs.tar.xz)
* [Sequence alignment files](C05_assets/aligned_seqs.tar.xz)

---

### There must be a better way!

[![AliView](C05_assets/aliview.png)](https://ormbunkar.se/aliview/)

* You will need to install Java, though!

```bash
sudo apt update
sudo apt install openjdk-8-jre
```

---

### DNA sequence databases

<ul>
<li class="fragment">[NCBI (USA)](https://www.ncbi.nlm.nih.gov/)</li>
<li class="fragment">[EMBL (Europe)](https://www.embl.org/)</li>
<li class="fragment">[DDBJ (Japan)](https://www.ddbj.nig.ac.jp)</li>
  <ul>
  <li class="fragment">Data repositories</li>
  <li class="fragment">Replicated</li>
  <li class="fragment">Queryable</li>
  </ul>
</ul>

<img class="fragment" src="C05_assets/data_center.jpg" style="background:none; border:none; box-shadow:none;">

---

### Obtaining sequences

<img class="fragment" src="C05_assets/psammodromus.jpg" style="background:none; border:none; box-shadow:none;">

<ul>
<li class="fragment">Meet *Psammodromus algirus*</li>
  <ul>
  <li class="fragment">Go to the [NCBI](https://www.ncbi.nlm.nih.gov/) website</li>
  <li class="fragment">Select the "nucleotide" database</li>
  <li class="fragment">Search for *Psammodromus algirus[organism], cytb[gene]*</li>
  </ul>
<li class="fragment">Get a file with all the sequences in FASTA format</li>
  <ul>
  <li class="fragment">Save it as `~/P_algirus.fasta`</li>
  </ul>
</ul>

---

### Using the NCBI Entrez API

<ul>
<li class="fragment">Called [E-utilities](https://www.ncbi.nlm.nih.gov/books/NBK25501/) or `eutils`</li>
<li class="fragment">Comprised of several functions like:</li>
  <ul>
  <li class="fragment">[esearch](https://www.ncbi.nlm.nih.gov/books/NBK25499/#chapter4.ESearch)</li>
  <li class="fragment">[epost](https://www.ncbi.nlm.nih.gov/books/n/helpeutils/chapter4/#chapter4.EPost)</li>
  <li class="fragment">[esummary](https://www.ncbi.nlm.nih.gov/books/NBK25499/#_chapter4_ESummary_)</li>
  <li class="fragment">[efetch](https://www.ncbi.nlm.nih.gov/books/n/helpeutils/chapter4/#chapter4.EFetch)</li>
  </ul>
<li class="fragment">Today we will only use `esearch` and `efetch`</li>
<li class="fragment">Base address: [https://eutils.ncbi.nlm.nih.gov/entrez/eutils](https://eutils.ncbi.nlm.nih.gov/entrez/eutils)</li>
  <ul>
  <li class="fragment">Try this in your browser: [https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/?db=nucleotide&term=Psammodromus algirus[organism], cytb[gene]](https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/?db=nucleotide&term=Psammodromus algirus[organism], cytb[gene])</li>
  </ul>
</ul>

|||

### Now try it from your shell!

```bash
# SPOILER ALERT!
# Scroll below for the solution














wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism], cytb[gene]" -O ~/Pal_Ids.xml

```

---

### What about getting some data?

<ul>
<li class="fragment">In order to get data, we use `efetch`</li>
<li class="fragment">`efetch` takes "arguments" such as a database, a list of IDs, and the desired type of return:</li>
  <ul>
  <li class="fragment">https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/?db=nucleotide&id=1252327351,1252327349,1252327347&rettype\=fasta</li>
  </ul>
<li class="fragment">This API call will retrieve 3 sequences in FASTA format</li>
<li class="fragment">Can you adapt it to get the full ID list from the *P. algirus* query?</li>
</ul>

|||

### A more dynamic approach

```bash
# SPOILER ALERT!
# Scroll below for the solution














ids=$(wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism], cytb[gene]"\&retmax=100 -O - |grep "^<Id>" | sed 's/[^0-9]//g' | tr "\n" "," |rev |cut -c 2- |rev)  # Get the list of IDs, comma separated
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/\?db\=nucleotide\&id\=$ids\&rettype\=fasta -O ~/P_algirus_cytb.fasta

```

---

### Using the "history" feature

<ul>
<li class="fragment">The "history" feature will store an ID list on NCBI's servers</li>
<li class="fragment">This is useful if:</li>
  <ul>
  <li class="fragment">We have a particularly large list of IDs</li>
  <li class="fragment">We want to further manipulate the search results</li>
  <li class="fragment">We want to create an "artificial" ID list</li>
  </ul>
<li class="fragment">*Entrez*'s "history" will provide us with 2 variables:</li>
  <ul>
  <li class="fragment">A *WebEnv*</li>
  <li class="fragment">A *Query Key*</li>
  </ul>
<li class="fragment">Just pass `usehistory=y` to `esearch`</li>
</ul>

|||

### History in practice

```bash
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi\?db\=nucleotide\&term\=Psammodromus%20algirus\[organism\],%20cytb\[gene\]\&usehistory\=y -O ~/Pal_history.xml
```

* Find both the *WebEnv* and the *Query Key* variables in the resulting XML

---

### Using "history" part II

<ul>
<li class="fragment">Now that we have a stored list of IDs, how do we access it?</li>
<li class="fragment">Using `efetch`, with the following parameters:</li>
  <ul>
  <li class="fragment">`db=nucleotide`</li>
  <li class="fragment">`usehistory=y`</li>
  <li class="fragment">`query_key=${key}`</li>
  <li class="fragment">`WebEnv=${webenv}`</li>
  <li class="fragment">`rettype=fasta`</li>
  </ul>
<li class="fragment">Try it!</li>
</ul>

---

### Your task for today:

<ul>
<li class="fragment">Write a small script to retrieve sequences that:</li>
  <ul>
  <li class="fragment">Uses the *Entrez* API</li>
  <li class="fragment">Allows the user to choose which database to query</li>
  <li class="fragment">Allows the user to pass a search term</li>
  <li class="fragment">Uses the "history" API feature</li>
  <li class="fragment">The result has to be in FASTA format and sent to `STDOUT`</li>
  </ul>
<li class="fragment">Organize in groups of up to 3 elements</li>
<li class="fragment">Programming language and tools are up to you</li>
<li class="fragment">Document how it is used!</li>
</ul>

---

### References

* [IUPAC reference](https://www.bioinformatics.org/sms/iupac.html)
* [How Sanger sequencing works](https://www.thermofisher.com/us/en/home/life-science/sequencing/sanger-sequencing/sanger-sequencing-workflow.html)
* [NCBI search terms list](https://www.ncbi.nlm.nih.gov/books/NBK49540/)
* [E-utilities quick start guide](https://www.ncbi.nlm.nih.gov/books/NBK25500/)
* [E-utilities full documentation](https://eutils.ncbi.nlm.nih.gov/)
