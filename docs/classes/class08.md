### classes[7] = "Phylogenetics Pt. III - Distance methods + Parsimony"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">Phylogenetic resconstruction methods</li>
<li class="fragment">Maximum parsimony</li>
<li class="fragment">Distance methods</li>
<li class="fragment">Practical work</li>
</ul>

---

### Phylogenetic reconstruction methods

<ul>
<li class="fragment" data-fragment-index="1">There are several methods for phylogenetic reconstruction</li>
<li class="fragment" data-fragment-index="2">Those that use character states</li>
  <ul>
  <li class="fragment" data-fragment-index="3"><span class="fragment highlight-red" data-fragment-index="9">Maximum parsimony</span></li>
  <li class="fragment" data-fragment-index="4">Maximum likelihood</li>
  <li class="fragment" data-fragment-index="5">Bayesian inference</li>
  </ul>
<li class="fragment" data-fragment-index="6">Those that use distance measurements</li>
  <ul>
  <li class="fragment" data-fragment-index="7"><span class="fragment highlight-red" data-fragment-index="10">UPGMA</span></li>
  <li class="fragment" data-fragment-index="8"><span class="fragment highlight-red" data-fragment-index="10">Neighbour joining</span></li>
  </ul>
</ul>

---

### Parsimony methods

<ul>
<li class="fragment">Assumes the minimum possible changes</li>
<li class="fragment">It uses this premiss to perform phylogenetic reconstruction</li>
</ul>

|||

### Maximum parsimony

<img src="C08_assets/MP_01.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_02.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_03.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_04.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_05.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_06.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_07.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="C08_assets/MP_08.png" style="background:none; border:none; box-shadow:none;">

---

### Consensus trees

<ul>
<li class="fragment">What if there are ties for the best scoring tree?</li>
</ul>
<img src="C08_assets/Consensus_01.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Strict consensus

<img src="C08_assets/Consensus_02.png" style="background:none; border:none; box-shadow:none;">

|||

### Majority Rule consensus

<img src="C08_assets/Consensus_03.png" style="background:none; border:none; box-shadow:none;">

|||

### Consensus trees

<ul>
<li class="fragment">Strict</li>
  <ul>
  <li class="fragment">Groups that occur on all trees</li>
  </ul>
<li class="fragment">Majority Rule</li>
  <ul>
  <li class="fragment">Groups that occur on *X%* (50 - 99) trees</li>
  </ul>
<li class="fragment">Semi-Strict</li>
<li class="fragment">Adams</li>
<li class="fragment">...</li>
</ul>

---

### Finding the best tree

<img src="C08_assets/Possible_trees.png" style="background:none; border:none; box-shadow:none;">

|||

### Finding the best tree

<ul>
<li class="fragment">In order to find the most parsimonious tree, all have to be tried</li>
  <ul>
  <li class="fragment">...unless we "cheat"</li>
  </ul>
<li class="fragment">In order to save computation time some shortcuts have been devised:</li>
  <ul>
  <li class="fragment">Heuristic search</li>
  <li class="fragment">Branch-and-bound search</li>
  <li class="fragment">Quartets search</li>
  </ul>
<li class="fragment">These methods sacrifice the guarantee of find the best tree</li>
<li class="fragment">In exchange for shortening computation time</li>
</ul>

---

### Distance methods

<ul>
<li class="fragment">Count the number of differences between taxa</li>
  <ul>
  <li class="fragment">**Extremely** fast</li>
  <li class="fragment">**Extremely** inaccurate</li>
  <li class="fragment">Discard a lot of information</li>
  </ul>
<li class="fragment">Can be corrected with sequence evolution models</li>
</ul>

|||

### Distance methods

<img src="C08_assets/dist01.png" style="background:none; border:none; box-shadow:none;">

|||

### Distance methods

<ul>
<li class="fragment">Once the distance matrix is calculated, several algorithms can be used to build a tree</li>
  <ul>
  <li class="fragment">Neighbour-Joining (NJ)</li>
  <li class="fragment">**U**nweighted **P**air **G**roup **M**ethod with **A**rithmetic mean (UPGMA)</li>
<li class="fragment">The choice of algorithm can heavily influence the obtained tree!</li>
</ul>

---

### Practical work

* Install the software [MEGA X](https://www.megasoftware.net/)
* Build a Maximum Parsimony tree on each of the 3 gene fragments used in Akihito *et al.* 2016
* Interpret the trees (consult the original paper if you have to)
* Build *p-distance* trees based on the same data
* Interpret these tree and compare them to the MP ones
* You can find a copy of the data [here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C08_assets/Akihito_data.tar.xz) in case you have lost yours for some reason

---

### References

* [A very detailed paper on Maximum Parsimony](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3377548/)
* [A review paper on consensus trees](http://info.univ-angers.fr/~gh/Idas/Wphylog/refart/ConsensusAMS.pdf)
* [Details on tree searching methods](https://www.bio.fsu.edu/~stevet/BSC5936/Swofford.F2003.2.pdf)
* [Phylogenetic inference based on distance methods](https://www2.ib.unicamp.br/profs/sfreis/SistematicaMolecular/Aula08MetodosMatrizesDistancias/Leituras/ThePhylogeneticHandbookMatrizesDistancias.pdf)
* [MEGA Manual](https://www.megasoftware.net/web_help_10/index.htm#t=First_Time_User.htm)

