#!/usr/bin/Rscript

likelihood = rbeta(10000000, 2, 7440)
plot(density(likelihood), type="l",
     lwd="2", col="red", xlab="Probability of success",
     main="C3PO's data records")

prior = rbeta(10000000, 20000, 1)
plot(density(prior), type="l",
     lwd="2", col="blue", xlab="Probability of success",
     main="Our belief that Han will succeed")

posterior = rbeta(10000000, 20002, 7441)
plot(density(posterior), type="l",
     xlim=c(0,1), lwd="2", col="forestgreen",
     xlab="Probability of success",
     main="Posterior probability of success")
