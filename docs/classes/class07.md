### classes[6] = "Phylogenetics Pt. II - Alignments"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">Why alignments</li>
<li class="fragment">Alignments algorithms</li>
<li class="fragment">Which to use and when (blast, mafft & clustal, vsearch)</li>
<li class="fragment">Practical work</li>
</ul>

---

### Alignment concept

<ul>
<li class="fragment">Phylogenetic analysis are about performing comparisons</li>
<li class="fragment">We have to be sure what we have is comparable</li>
  <ul>
  <li class="fragment"><a href="https://en.wikipedia.org/wiki/Homology_(biology)">*Homology*</a> is required</li>
  <li class="fragment">Each of our *characters* has to be homologous</li>
  </ul>
</ul>

<img class="fragment" src="C07_assets/unalign_vs_align.png" style="background:none; border:none; box-shadow:none;">

---

### Sequence variation

<ul>
<li class="fragment">Sequences may diverge from a **common ancestor** in 3 different ways</li>
  <ul>
  <li class="fragment">Substitutions: (<font color="green">ACT<font color="red">G</font> <font color="navy">-></font> ACT<font color=red>C</font></font>)</li>
  <li class="fragment">Insertions: (<font color="green">ACTG <font color="navy">-></font> ACTG<font color=red>TTGG</font></font>)</li>
  <li class="fragment">Deletion: (<font color="green">ACTG <font color="navy">-></font> A</font>)</li>
  </ul>
<li class="fragment">Which phenomena results in gaps?</li>
</ul>

---

### Scoring alignments

<ul>
<li class="fragment">Let's try to align "ALI" and "IGN"</li>
</ul>

<span class="fragment">

<pre class="fragment">
ALI | -ALI | --ALI | ALI- | ALI-- | A-LI | AL-I | ALI- | ALI- 
IGN | IGN- | IGN-- | -IGN | --IGN | IGN- | IGN- | I-GN | IG-N ...
</pre>

</span>

<ul>
<li class="fragment">Each alignment has to be scored</li>
</ul>

<img class="fragment" src="C07_assets/newhighscore.jpg" style="background:none; border:none; box-shadow:none;">

|||

### Scoring alignments

<ul>
<li class="fragment">Gap penalty function</li>
  <ul>
  <li class="fragment">*w*(*k*) indicates the cost of opening a gap of length *k*</li>
  </ul>
<li class="fragment">Substitution matrix</li>
  <ul>
  <li class="fragment">*s*(*a*,*b*) indicates the cost of aligning *a* with *b*</li>
  </ul>
<li class="fragment">Different algorithms will score these in different ways</li>
</ul>

---

### Alignment algorithms

<ul>
<li class="fragment">There is a plethora of available alignment algorithms</li>
  <ul>
  <li class="fragment">Smith-Waterman</li>
  <li class="fragment">BLAST</li>
  <li class="fragment">Clustal</li>
  <li class="fragment">vsearch</li>
  <li class="fragment">mafft</li>
  </ul>
<li class="fragment">There are many, many other</li>
<li class="fragment">All have a place (some in history)</li>
</ul>

<img class="fragment" src="C07_assets/trophy.jpg" style="background:none; border:none; box-shadow:none;">

|||

### Alignment algorithms

<ul>
<li class="fragment">Smith-Waterman</li>
  <ul>
    <li class="fragment">Find optimum local alignments (slow & accurate)</li>
  </ul>
  <li class="fragment">BLAST</li>
  <ul>
    <li class="fragment">Heuristic approach to S-W (faster, at cost of accuracy)</li>
  </ul>
  <li class="fragment">Clustal</li>
  <ul>
    <li class="fragment">Progressive alignment - slow, inaccurate when dealing with multiple gaps</li>
  </ul>
  <li class="fragment">vsearch</li>
  <ul>
    <li class="fragment">Fast & accurate, ideal for finding optimum local alignments</li>
  </ul>
  <li class="fragment">mafft</li>
  <ul>
    <li class="fragment">Iterative method with consistency based scoring approach - fast & accurate</li>
  </ul>
</ul>

---

### Who and when?

<ul>
<li class="fragment">Find a sequence's best match to a large pool of candidates</li>
  <ul>
  <li class="fragment">Smith-Waterman</li>
  <li class="fragment">BLAST</li>
  <li class="fragment">vsearch</li>
  </ul>
<li class="fragment">Align multiple sequences</li>
  <ul>
  <li class="fragment">Clustal</li>
  <li class="fragment">mafft</li>
  <li class="fragment">muscle</li>
  </ul>
</ul>

---

### Your task for today

* Obtain all sequences used in [*Akihito et al. 2016*](https://www.sciencedirect.com/science/article/pii/S0378111915012226)
 * You will find this paper in Moodle
* Align these sequences in 3 separate FASTA files - one per gene fragment
* *Hint #1:* You can use the fields [author] and [year] in the *Entrez* search string
* *Hint #2:* Use *mafft* to align your sequences

<img class="fragment" src="C07_assets/japan.jpg" style="background:none; border:none; box-shadow:none;">

---

### References

* [Homology concept (Wikipedia)](https://en.wikipedia.org/wiki/Homology_(biology))
* [Mutation types](https://evolution.berkeley.edu/evolibrary/article/mutations_03)
* [Sequence alignment "behind the scenes"](https://www.bioinformaticshome.com/bioinformatics_tutorials/sequence_alignment/DNA_scoring_matrices.html)
* [*Akihito et al. 2016*](https://www.sciencedirect.com/science/article/pii/S0378111915012226)
* [BLAST algorithm](https://blastalgorithm.com/)
* [mafft (and references therein)](https://mafft.cbrc.jp/alignment/software/)

