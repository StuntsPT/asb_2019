### classes[5] = "Phylogenetics Pt. I"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">What is phylogenetics</li>
<li class="fragment">Revisiting DNA</li>
<li class="fragment">Phylogenetics history</li>
<li class="fragment">Phylogenetics concepts</li>
</ul>

<img class="fragment" src="C06_assets/generic_phylo.jpg" style="background:none; border:none; box-shadow:none;">

---

### What is phylogenetics?

<ul>
<li class="fragment">A set of methods to infer relations between OTUs</li>
<li class="fragment">It's used in:</li>
  <ul>
  <li class="fragment">Taxonomy</li>
  <li class="fragment">Molecular dating</li>
  <li class="fragment">Molecular evolution</li>
  <li class="fragment">Gene transfer detection</li>
  </ul>
</ul>

<img class="fragment" src="C06_assets/darwin.jpg" style="background:none; border:none; box-shadow:none;">

---

### What do we need for phylogenetics?

<ul>
<li class="fragment">Many different types of data:</li>
  <ul>
  <li class="fragment">Morphological traits</li>
  <li class="fragment">Behavioural traits</li>
  <li class="fragment">Molecular sequences</li>
    <ul>
    <li class="fragment">Amino Acid sequences</li>
    <li class="fragment"><span class="fragment highlight-red">DNA sequences</span></li>
    </ul>
  </ul>
</ul>

<img class="fragment" src="C06_assets/chroma.png" style="background:none; border:none; box-shadow:none;">

---

### Remembering DNA

---

### DNA

<img src="C06_assets/DNA.jpg" style="background:none; border:none; box-shadow:none;">

<p class="fragment">**D**eoxyribo**n**ucleic **a**cid</p>

---

### Where can we find DNA?

<img src="C06_assets/animal-cell.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Where can we find DNA?

<img src="C06_assets/plant-cell.jpg" style="background:none; border:none; box-shadow:none;">

---

### Where can we find DNA?

<img src="C06_assets/bacteria-cell.jpg" style="background:none; border:none; box-shadow:none;">

---

### Where can we find DNA?

<img src="C06_assets/virus.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C06_assets/Cell01.png" data-background-size="1024px">

---

<section data-background="C06_assets/Cell02.png" data-background-size="1024px">

---

### Nucleus

<img src="C06_assets/nucleus.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C06_assets/Cell03.png" data-background-size="1024px">

---

### Mitochondria

<img src="C06_assets/mitochondria.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C06_assets/Cell04.png" data-background-size="1024px">

---

### Ribosome

<img src="C06_assets/ribosome_01.jpg" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C06_assets/Cell05.png" data-background-size="1024px">

---

### DNA structure

---

<section data-background="C06_assets/Chemical-structure-of-DNA.png" data-background-size="1024px">

---

<section data-background="C06_assets/4-nucleotides.png" data-background-size="1024px">

---

<section data-background="C06_assets/5-nucleotides.png" data-background-size="1024px">

---

<section data-background="C06_assets/Phospahte-backbone.png" data-background-size="1024px">

---

<section data-background="C06_assets/Watson-Crick-pair-rule.png" data-background-size="1024px">

---

<section data-background="C06_assets/Sense-DNA.png" data-background-size="1024px">

---

<section data-background="C06_assets/Sense-template.png" data-background-size="1024px">

---

### Sequencing DNA

Sanger Method

<img src="C06_assets/sanger.jpg" style="background:none; border:none; box-shadow:none;">

|||

### Sequencing DNA

Illumina method

<img src="C06_assets/Illumina.png" style="background:none; border:none; box-shadow:none;">

|||

### Sequencing DNA

Oxford Nanopore

<img src="C06_assets/nanopore.jpg" style="background:none; border:none; box-shadow:none;">

---

### Mitochondrial DNA

<img src="C06_assets/human_mito_map.png" width="40%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">


<ul>
<li class="fragment">Circular</li>
<li class="fragment">~17Kbp</li>
<li class="fragment">Independent replication</li>
<li class="fragment">No recombination</li>
<li class="fragment">Maternal inheritance</li>
<li class="fragment">No repair</li>
<li class="fragment">High mutation rate</li>
</ul>

---

### Phylogenetics history

---

### Cladistics

<img src="C06_assets/Hennig.jpg" width="20%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<ul style="width: 70%;">
<li class="fragment">Willi Hennig</li>
  <ul>
  <li class="fragment">*Phylogenetic Systematics*</li>
  <li class="fragment">1955</li>
  </ul>
<li class="fragment">Introduces the concept of "cladograms"</li>
  <ul>
  <li class="fragment">Show relations between taxa</li>
  <li class="fragment">Grouping is based on traits</li>
  <li class="fragment">Similarities are assumed to be of common ancestry</li>
  </ul>
<img src="C06_assets/cladogram.png" width="30%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -250px;right: -10px;">
</ul>

---

### Phylogenetics

<img src="C06_assets/CS-E.jpg" width="20%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<ul style="width: 70%;">
<li class="fragment">Cavalli-Sforza & Edwards</li>
  <ul>
  <li class="fragment">*Phylograms*</li>
  <li class="fragment">1963</li>
  </ul>
</ul>

<img src="C06_assets/CS_phylo.jpg" width="30%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: 0px;right: 220px;">
<img src="C06_assets/CS_migrations.jpg" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -150px;left: -30px;">

---

### Phylogenetic concepts

<img class="fragment" src="C06_assets/Phylo_01.png" style="background:none; border:none; box-shadow:none;">

|||

### Phylogenetic concepts

<img class="fragment" src="C06_assets/Phylo_02.png" style="background:none; border:none; box-shadow:none;">

---

### Rooted Vs. Unrooted Trees

<img class="fragment" src="C06_assets/tree_roots.png" style="background:none; border:none; box-shadow:none;">

---

### Endgame

[<img class="fragment" src="C06_assets/pan-aves.jpg" style="background:none; border:none; box-shadow:none;">](C06_assets/pan-aves_large.jpg)

<p class="fragment">(click for larger version)</p>

---

### References

* [Mitochondrial DNA](https://ghr.nlm.nih.gov/mitochondrial-dna#)
* [Molecular phylogenetics for newcommers](https://link.springer.com/chapter/10.1007%2F10_2016_49) (Find the PDF on Moodle)
* [Rooted Vs. Unrooted trees](https://www.ncbi.nlm.nih.gov/Class/NAWBIS/Modules/Phylogenetics/phylo9.html)
* [Theropoda evolution](http://paleoitalia.org/archives/file/bollettino-spi/507 )
