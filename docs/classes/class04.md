### Linux shell 201

![Linux](C03_assets/linux.jpg)

© Francisco Pina Martins 2015-2019

---

### Now that you know basic shell:

<ul>
<li class="fragment">Scripting</li>
  <ul>
  <li class="fragment">Running</li>
  <li class="fragment">Reading</li>
  <li class="fragment">Writing</li>
  </ul>
<li class="fragment">Privileges</li>
  <ul>
  <li class="fragment">Who</li>
  <li class="fragment">How</li>
  </ul>
<li class="fragment">Package managers</li>
  <ul>
  <li class="fragment">System-level</li>
  <li class="fragment">User level</li>
  </ul>
</ul>

---

### Scripting

<ul>
<li class="fragment">Running a script</li>
<li class="fragment">Try to do this on your own:</li>
  <ul>
  <li class="fragment">Get [this script](C04_assets/hello.sh) in your file system</li>
  <li class="fragment">Run it</li>
  <li class="fragment">It's OK if you run into some trouble</li>
  </ul>
</ul>

<img src="C04_assets/obi-wan.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Scripting

```bash
# SPOILER ALERT!
# Scroll below for the solution















wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C04_assets/hello.sh
chmod +x hello.sh
./hello.sh

```

---

### Scripting

<ul>
<li class="fragment">Let's up the game</li>
<li class="fragment">Try to do the same for these scripts</li>
  <ul>
  <li class="fragment">[Python](C04_assets/hello.py)</li>
  <li class="fragment">[Perl](C04_assets/hello.pl)</li>
  <li class="fragment">[Ruby](C04_assets/hello.rb)</li>
  <li class="fragment">[JavaScript](C04_assets/hello.js)</li>
  </ul>
<li class="fragment">Notice something strange?</li>
</ul>

<img src="C04_assets/grevious.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Scripting

```bash
# SPOILER ALERT!
# Scroll below for the solution















wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C04_assets/hello.py
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C04_assets/hello.pl
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C04_assets/hello.rb
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C04_assets/hello.js
chmod +x hello.*
./hello.py
./hello.pl
./hello.rb  # Something went wrong!
./hello.js  # Something went wrong again!

```

---

### Looking at scripts

<ul>
<li class="fragment">Look at these scripts using some shell tools</li>
  <ul>
  <li class="fragment">`cat`</li>
  <li class="fragment">`less`</li>
  <li class="fragment">`nano`</li>
  <li class="fragment">`vim`</li>
  </ul>
</ul>

<img src="C04_assets/grevious2.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Plot twist!

<ul>
<li class="fragment">Try to compress all the scripts in a `tar.xz` file</li>
<li class="fragment">You will need:</li>
  <ul>
  <li class="fragment">`tar`</li>
  <li class="fragment">wildcards</li>
  </ul>
<li class="fragment">Try to uncompress them afterwards</li>
</ul>

|||

### Plot twist!

```bash
# SPOILER ALERT!
# Scroll below for the solution















tar cvfJ scripts.tar.xz hello.*  # This will compress your files in a tar.xz archive
mkdir compression_tests
mv scripts.tar.xz compression_tests
cd compression_tests
tar xvfJ scripts.tar.xz  # This will uncompress them
```

---

### Shebang (AKA Hashbang)

<ul>
<li class="fragment">Notice the `#!` in the first line?</li>
  <ul>
  <li class="fragment">This is called a "Shebang"</li>
  <li class="fragment">It tells your shell which program should *interpret* the script</li>
  <li class="fragment">Without it you would need to tell this to the shell</li>
    <ul>
    <li class="fragment">`perl hello.pt`</li>
    </ul>
  </ul>
</ul>

<a href="https://www.youtube.com/watch?v=5ihtX86JzmA&t=1m14s">
<img src="C04_assets/ricky.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">
</a>

---

### Your turn!

<ul>
<li class="fragment">Write a small script that does the following for the python and perl scripts:</li>
  <ul>
  <li class="fragment">Prints the file name to the console</li>
  <li class="fragment">Prints the contents of the script to the console</li>
  <li class="fragment">Prints an empty newline</li>
  </ul>
<li class="fragment">How could you save that information in a file?</li>
<li class="fragment">How could you filter only the "Shebang" (#!) lines?</li>
</ul>

|||

### Your turn

```bash
# SPOILER ALERT!
# Scroll below for the solution














#!/bin/bash
ls hello.p*
cat hello.p*
```

|||

### Your turn

```bash
# SPOILER ALERT!
# Scroll below for the solution














chmod +x my_script.sh
./my_script.sh
./my_script.sh > my_text.txt
./my_script |grep "^#"
```

---

### Privileges

<ul>
<li class="fragment">Remember that users can only **write** on their *home* dir?</li>
<li class="fragment">What if we need to write files somewhere else?</li>
  <ul>
  <li class="fragment">Only the `root` user can do that</li>
    <ul>
    <li class="fragment">You need to become `root`</li>
    <li class="fragment">Or run a command with `root` privileges</li>
    </ul>
  </ul>
<li class="fragment" style="color:red">For security reasons, you should not use the `root` account for any longer then necessary</li>
</ul>

<img src="C04_assets/groot.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Escalating privileges

<ul>
<li class="fragment">In order to become `root` you have to switch to the `root` "user"</li>
  <ul>
  <li class="fragment">Just use `su`</li>
    <ul>
    <li class="fragment">This will not work on Ubuntu by deafult</li>
    <li class="fragment">The `root` account is *disabled*</li>
    </ul>
  </ul>
<li class="fragment">How do we do this?</li>
</ul>

<img src="C04_assets/how.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### sudo

<ul>
<li class="fragment">`sudo` is the answer</li>
  <ul>
  <li class="fragment">Stands for "superuser do"</li>
    <ul>
    <li class="fragment">`sudo <command>`</li>
    </ul>
  <li class="fragment">Allows an *unprivileged* user to run a command with elevated privileges</li>
  <li class="fragment">Asks for the user authentication</li>
    <ul>
    <li class="fragment">Provided said user is *authorized* to use `sudo`</li>
    <li class="fragment">Usually authorized users are part of the `sudo` group</li>
    </ul>
  </ul>
<li class="fragment" style="color:red">`sudo` can be considered a weapon of mass destruction if used carelessly</li>
<li class="fragment">[Mandatory XKCD](https://www.xkcd.com/149/)</li>
</ul>

---

### Updating our system

<ul>
<li class="fragment">An example of a task that requires elevated privileges is performing an update to our system</li>
<li class="fragment">You have to write the updated files to system dirs</li>
  <ul>
  <li class="fragment">The commands are:</li>
    <ul>
    <li class="fragment">apt update</li>
    <li class="fragment">apt dist-upgrade</li>
    </ul>
  <li class="fragment">Your user cannot do this...</li>
  </ul>
</ul>

|||

### Updating our system

```bash
# SPOILER ALERT!
# Scroll below for the solution














sudo apt update
sudo apt dist-upgrade
```

---

### Package managers!

---

### What does a package manager do?

<ul>
<li class="fragment">The "correct" way to manage programs on a GNU/linux system</li>
<li class="fragment">They will handle all of the process</li>
  <ul>
  <li class="fragment">Installing programs</li>
  <li class="fragment">Removing programs</li>
  <li class="fragment">Updating progrms</li>
  </ul>
  <li class="fragment">Keep track of all this activity</li>
</ul>

---

### Package manager concepts

<ul>
<li class="fragment">Packages</li>
  <ul>
  <li class="fragment">Single file containg the software and metadata</li>
  <li class="fragment">`.deb`, `.rpm`, `.tar.xz`</li>
  </ul>
<li class="fragment">Dependencies</li>
  <ul>
  <li class="fragment">Some packages need others to work</li>
  <li class="fragment">Eg. `numpy` depends on `python`</li>
  </ul>
<li class="fragment">Package database keeps track of:</li>
  <ul>
  <li class="fragment">All installed packages/files</li>
  <li class="fragment">All available pacakges</li>
  <li class="fragment">All instaled packages' metadata</li>
  </ul>
<li class="fragment">Repositories</li>
  <ul>
  <li class="fragment">Store remote pacakges</li>
  <li class="fragment">Store remote databases</li>
  </ul>
</ul>

---

### Using a package manager

* DB-sync <!-- .element: class="fragment" data-fragment-index="1" -->
* Update packages <!-- .element: class="fragment" data-fragment-index="2" -->
* Install packages <!-- .element: class="fragment" data-fragment-index="3" -->
* Remove packages <!-- .element: class="fragment" data-fragment-index="4" -->

|||

### Using a package manager

```bash
# SPOILER ALERT!
# Scroll below for the solution















sudo apt update  # DB-sync

sudo apt dist-upgrade  # Update pacakges

sudo apt install vim  # Installs Vim
sudo apt install ruby nodejs # Installs ruby and node


# Install xterm, test it and remove it
sudo apt install xterm 

# Run xterm to test it

sudo apt remove xterm
```

---

### Plot twist #2:

* Now that you have installed `ruby` and `node`, try to run the scripts that previously failed 

---

### User space package managers

* pip <!-- .element: class="fragment" data-fragment-index="1" -->
* cran <!-- .element: class="fragment" data-fragment-index="2" -->
* npm <!-- .element: class="fragment" data-fragment-index="3" -->
* conda <!-- .element: class="fragment" data-fragment-index="4" -->

---

### Focus on [conda](https://conda.io)

<ul>
<li class="fragment">A package, dependency and environment manager for any language</li>
<li class="fragment">Used to install packages, without requiring root access</li>
<li class="fragment">Can be used to manage isolated environments</li>
<li class="fragment">[Simple to install](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html)</li>
<li class="fragment">[Bioconda](https://bioconda.github.io/)</li>
</ul>

---

### Your turn

<ul>
<li class="fragment">Install `conda` in your VM</li>
<li class="fragment">Add the `Bioconda` channel to the "repositories"</li>
<li class="fragment">Install the program `fastqc`</li>
</ul>

---

### More than just a package manager

<ul>
  <li class="fragment">`Conda` is much more than just a package manager</li>
  <li class="fragment">It can be used to create and manage isolated environments</li>
  <li class="fragment">Can be used to manage isolated environments:</li>
  <ul>
    <li class="fragment">`conda create -n yourenvname python=3.3 anaconda`</li>
  </ul>
  <li class="fragment">You can then activate each of your environments:</li>
  <ul>
    <li class="fragment">`source activate yourenvname`</li>
  </ul>
  <li class="fragment">Programs from one env cannot "see" or use programs from another env</li>
</ul>

|||

### More than just a package manager


```bash
python --version

conda env list
conda create -n oldsnake python=3.3

conda env list
source activate oldsnake

python --version

source deactivate
conda env remove -n oldsnake

```

---

### Your task:

* Create a new conda environment and install `ipyrad`
* Make sure `ipyrad` is running: `ipyrad -v`
* Check the version of `python` in your `$PATH`
* Now create a new conda environement and install `vcfpy`
* Mare sure it runs by issuing the `vcfpy` terminal command
* Check the version of `python` in your `$PATH`
* What can you conclude from your experience?

---

### References

* [Shell scripting tutorial](https://www.guru99.com/introduction-to-shell-scripting.html)
* [Privilege elevation in GNU/Linux](https://www.linux.com/learn/linux-101-introduction-sudo)
* [Package manager (wikipedia)](https://en.wikipedia.org/wiki/Package_manager)
* [apt-get documentation](https://help.ubuntu.com/community/AptGet/Howto)
* [Conda documentation](https://docs.conda.io/en/latest/)
