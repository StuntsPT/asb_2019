#### classes[1] = "Virtualizing hardware"

##### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Hardware 101

* Typical x86 PC case internals <!-- .element: class="fragment" data-fragment-index="1" -->
  * Motherboard <!-- .element: class="fragment" data-fragment-index="2" -->
  * CPU/APU <!-- .element: class="fragment" data-fragment-index="3" -->
  * GPU <!-- .element: class="fragment" data-fragment-index="4" -->
  * RAM <!-- .element: class="fragment" data-fragment-index="5" -->
  * Storage <!-- .element: class="fragment" data-fragment-index="6" -->
  * PSU <!-- .element: class="fragment" data-fragment-index="7" -->
  * Network card(s) <!-- .element: class="fragment" data-fragment-index="8" -->
  * Other I/O ports <!-- .element: class="fragment" data-fragment-index="9" -->

---

### Take a few minutes to look at one

---

### Hardware meets software

<ul>
<li class="fragment">A Virtual Machine (VM)</li>
  <ul>
  <li class="fragment">Is "a PC inside your PC"</li>
    <ul>
    <li class="fragment">The "real" PC is the *Host*</li>
    <li class="fragment">The "virtual" PC is the *Guest*</li>
    </ul>
  <li class="fragment">Most of the *guest* "hardware" is software defined</li>
  <li class="fragment">Some parts are directly passed from *host* to *guest*</li>
  </ul>
</ul>

---

### What is this for?

* Running an **unmodified** *guest* OS on the *host* system
* Running multiple OSes in the same machine <!-- .element: class="fragment" data-fragment-index="1" -->
* Spawning "new" machines on demand using a single physical machine <!-- .element: class="fragment" data-fragment-index="2" -->

---

### VM advantages over physical systems

* Spawned on demand
* Movable between physical machines <!-- .element: class="fragment" data-fragment-index="1" -->
* Easy to copy <!-- .element: class="fragment" data-fragment-index="2" -->
* Snapshot support <!-- .element: class="fragment" data-fragment-index="3" -->
* Easily scalable <!-- .element: class="fragment" data-fragment-index="4" -->
* Security - code is confined <!-- .element: class="fragment" data-fragment-index="5" -->

---

### VM limitations

* *Host* and *guest* code must share the same architecture
  * Unless you use emulation <!-- .element: class="fragment" data-fragment-index="1" -->
* Has some performance hit <!-- .element: class="fragment" data-fragment-index="2" -->
* No GPU acceleration <!-- .element: class="fragment" data-fragment-index="3" -->
  * Experimental support for GPU pass-through <!-- .element: class="fragment" data-fragment-index="3" -->
* Resource sharing <!-- .element: class="fragment" data-fragment-index="4" -->

---

### *Host* system

* x86_64 architecture  <!-- .element: class="fragment" data-fragment-index="1" -->
* MS Windows 10 (64bit) OS <!-- .element: class="fragment" data-fragment-index="2" -->
* 8Gb RAM <!-- .element: class="fragment" data-fragment-index="3" -->
* Large SSD <!-- .element: class="fragment" data-fragment-index="4" -->
* Defines resource sharing <!-- .element: class="fragment" data-fragment-index="5" -->
* VirtualBox 5.X <!-- .element: class="fragment" data-fragment-index="6" -->

|||

### VirtualBox

* Virtualization software <!-- .element: class="fragment" data-fragment-index="1" -->
* Mostly open source <!-- .element: class="fragment" data-fragment-index="2" -->
  * Some optional proprietary parts <!-- .element: class="fragment" data-fragment-index="2" -->
* Very easy to use <!-- .element: class="fragment" data-fragment-index="3" -->
* GUI and CLI interfaces <!-- .element: class="fragment" data-fragment-index="4" -->
* Alternatives <!-- .element: class="fragment" data-fragment-index="5" -->
  * VMware <!-- .element: class="fragment" data-fragment-index="5" -->
  * KVM <!-- .element: class="fragment" data-fragment-index="5" -->
  * XEN <!-- .element: class="fragment" data-fragment-index="5" -->
  * Hyper-V <!-- .element: class="fragment" data-fragment-index="5" -->

---

### *Guest* system

<ul>
<li class="fragment">x86_64 architecture</li>
<li class="fragment">[Ubuntu](https://www.ubuntu.com/) (64 bit) OS</li>
<li class="fragment">4Gb RAM</li>
<li class="fragment">Small storage device</li>
<li class="fragment">Can seamlessly scale resources</li>
</ul>


|||

### What is Ubuntu?

* GNU/Linux based OS <!-- .element: class="fragment" data-fragment-index="1" -->
* Designed to be easy to use <!-- .element: class="fragment" data-fragment-index="2" -->
* New release every 6 months <!-- .element: class="fragment" data-fragment-index="3" -->
  * 9 months support <!-- .element: class="fragment" data-fragment-index="3" -->
* Long term support every 2 years <!-- .element: class="fragment" data-fragment-index="4" -->
  * 5 years support <!-- .element: class="fragment" data-fragment-index="4" -->
* All code is Open Source <!-- .element: class="fragment" data-fragment-index="5" -->

---

### Create your own VM

* HDD - 20Gb <!-- .element: class="fragment" data-fragment-index="1" -->
  * Will be "partitioned" <!-- .element: class="fragment" data-fragment-index="2" -->
* RAM - 4Gb <!-- .element: class="fragment" data-fragment-index="3" -->

|||

### A word about storage in GNU/Linux systems

* ["Everything is a file"](https://en.wikipedia.org/wiki/Everything_is_a_file)
* /dev/{hd,sd,nvme}X <!-- .element: class="fragment" data-fragment-index="1" -->
* Mountpoints <!-- .element: class="fragment" data-fragment-index="2" -->

---

### Goal for today

Create a VM, install [Ubuntu](https://www.ubuntu.com/)

[Download link](http://ftp.rnl.tecnico.ulisboa.pt/pub/ubuntu/releases/cosmic/ubuntu-18.10-desktop-amd64.iso)

---

## Now, we go live!

---

### References

* [Virtual machine on wikipedia](https://en.wikipedia.org/wiki/Virtual_machine)
* [Why, When, and How To Use a Virtual Machine - The Linux foundation](https://www.linux.com/learn/why-when-and-how-use-virtual-machine)
* [What is Virtualizatio - VMWare](https://www.vmware.com/solutions/virtualization.html)
* [Ubuntu](https://www.ubuntu.com/)
* [Everything is a file](https://lwn.net/Articles/411845/)

