### Classes[12] = "Reduced Representation Libraries"

#### Análise de Sequências Biológicas 2019

![Logo EST](C01_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Summary

<ul>
<li class="fragment">What are RRLs?</li>
<li class="fragment">RRL types</li>
  <ul>
  <li class="fragment">What are RRLs useful for?</li>
  <li class="fragment">What are RRLs **not** useful for?</li>
<li class="fragment">RRL data analysis</li>
</ul>

---

### What are RRLs?

<ul>
<li class="fragment">A form of genomic *fingerprinting*</li>
  <ul>
  <li class="fragment">Resort to one (or more) restriction enzyme to "chop" the genome</li>
  <li class="fragment">*Reads* are produced from a limited number of genomic regions</li>
  <li class="fragment">*Reads* are identified</li>
  </ul>
<li class="fragment">An economic form of obtaining large batches of genomic variation</li>
</ul>

<img src="C13_assets/fingerprinting.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### The concept

<ol>
<li class="fragment">Reduce the genome</li>
<li class="fragment">Sequence target regions</li>
<li class="fragment">Assign sequences to samples</li>
<li class="fragment">Stack same region data</li>
<li class="fragment">Call variants between samples</li>
</ol>

|||

### How it works

<a href="C13_assets/floragenex-rad-dna-sequencing-bioinformatics-rad-diagram.png"><img src="C13_assets/floragenex-rad-dna-sequencing-bioinformatics-rad-diagram_small.png" style="background:none; border:none; box-shadow:none;" class="fragment"></a>

---

### Most common RRL types

<ul>
<li class="fragment">RAD-Seq</li>
  <ul>
  <li class="fragment">Same length barcodes</li>
  <li class="fragment">Max 48 individuals per run</li>
  <li class="fragment">Maximizes reads per sample</li>
  </ul>
<li class="fragment">GBS</li>
  <ul>
  <li class="fragment">Variable length barcodes</li>
  <li class="fragment">Max 96 individuals per run</li>
  <li class="fragment">Maximizes individuals per run</li>
  </ul>
</ul>

---

### RRL usage

<ul>
<li class="fragment">[Progenies](http://www.biomedcentral.com/1471-2164/14/566/abstract)</li>
<li class="fragment">[Linkage maps construction](http://www.biomedcentral.com/1471-2164/15/166)</li>
<li class="fragment">[Population Genomics](http://onlinelibrary.wiley.com/doi/10.1111/tpj.12370/abstract)</li>
<li class="fragment">[Demography](https://doi.org/10.1038/s41437-017-0037-y)</li>
<li class="fragment">[Association studies](https://doi.org/10.1111/gcb.14497)</li>
</ul>

---

### RRL non-usage

<ul>
<li class="fragment">Genome assembly</li>
</ul>

<img src="C13_assets/wrong1.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### RRL non-usage

<ul>
<li class="fragment">Gene expression analysis</li>
</ul>

<img src="C13_assets/wrong2.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### RRL non-usage

<ul>
<li class="fragment">Gene discovery</li>
</ul>

<img src="C13_assets/wrong3.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### In practice

1. Obtain data
2. *Read demultiplexing*

<img src="C13_assets/demultiplex.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### In practice

<ol start="3">
<li>*Loci* clustering per sample</li>
<li>*Loci* clustering multisample</li>
<li> Finding variants</li>
</ol>

<img src="C13_assets/clustering.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### In practice

<ol start = "6">
<li>Biological analyses</li>
</ol>

---

### Divide and conquer

* Divide the class in 2 teams
  * Team *ipyrad*
  * Team *Stacks*

<img src="C13_assets/pick_a_side.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Your task

* Get data from [here](https://gitlab.com/StuntsPT/small_test_dataset/raw/master/Lim_1M_R1.fastq.xz) and [here](https://gitlab.com/StuntsPT/small_test_dataset/raw/master/Lim_1M_R2.fastq.xz)
  * Paired GBS data
* Get the barcodes file from [here](https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C13_assets/Limonium_barcodes_axe.tsv)
* Use [Axe-demux](https://github.com/kdmurray91/axe) to demultiplex it
  * You have to COMPILE it from source
* Use your team's program to "assemble" the data
* The restriction overhang is `CGWC`
* Obtain a VCF file for easy comparison between teams
* Obtain a `phylip` file
* Make a ML phylogenetic tree

