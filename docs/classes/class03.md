### Linux shell 101

![Linux](C03_assets/linux.jpg)

© Francisco Pina Martins 2015-2019

---

### What is the shell?

<ul>
<li class="fragment">A "command line" interface</li>
<li class="fragment"> *bash* (*ksh* *zsh* *fish* *dash* etc...)</li>
<li class="fragment"> Can be accessed via a "terminal"</li>
<li class="fragment"> **Mostly** keyboard driven</li>
<li class="fragment"> The "mouse cursor" is also useful here</li>
</ul>

---

### Why do I need it?

<ul>
<li class="fragment">HPC clusters</li>
<li class="fragment">Remoto access</li>
<li class="fragment">**Simple** interface</li>
<li class="fragment">Process automation & reproducibility</li>
</ul>

---

## The *prompt*

``user@machine:~$``

``bash-4.1$``

![Prompt image](C03_assets/shell_prompt_small.gif)

[Pimp my prompt](https://www.cyberciti.biz/tips/howto-linux-unix-bash-shell-setup-prompt.html) <!-- .element: class="fragment" data-fragment-index="1" -->

---

### Navigation & orientation

<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

<ul>
<li class="fragment">Where am I?</li>
  <ul>
  <li class="fragment">``$ pwd`` </li>
  <li class="fragment"><font color="red">p</font>rint <font color="red">w</font>orking <font color="red">d</font>irectory </li>
  </ul>
<li class="fragment">What is in here? </li>
  <ul>
  <li class="fragment">``$ ls`` </li>
  <li class="fragment"><font color="red">l</font>i<font color="red">s</font>t </li>
  </ul>
<li class="fragment">How do I move around? </li>
  <ul>
  <li class="fragment">``$ cd`` </li>
  <li class="fragment"><font color="red">c</font>all <font color="red">d</font>irectory </li>
  </ul>
</ul>

---

### Your new best freinds


<ul>
<li class="fragment">``man program_name``</li>
<li class="fragment">**EVERY** shell *builtin* programs (coreutils) have it</li>
<li class="fragment">Some others have it too</li>
</ul>

<div class="fragment" style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

<ul>
<li class="fragment">``$ man ls``</li>
<li class="fragment">``$ whatis ls``</li>
</ul>


---

### Useful keyboard navigation keys

<ul>
<li class="fragment">"Tab" Key</li>
<div class="fragment" style="float: right"><img src="C03_assets/Tab-Key.png" /></div>
  <ul>
  <li class="fragment">Completes your command</li>
  <li class="fragment">Try with one or two "*keypresses*"
  </ul>
<li class="fragment">↓ and ↑ keys</li>
  <ul>
  <li class="fragment">Navigate previous commands</li>
  </ul>
</ul>

|||

### Useful keyboard navigation keys

```bash
cd

ls

ls Down<TAB>
ls D<TAB><TAB>

↑
```

---

### "The PATH"

<ul>
<li class="fragment">Paths can be absolute...</li>
  <ul>
  <li class="fragment">``/etc/fstab``</li>
  <li class="fragment">``~/Downloads/ficheiro.txt``</li>
  </ul>
<li class="fragment">Or relative...</li>
  <ul>
  <li class="fragment">``Downloads/ficheiro.txt``</li>
  <li class="fragment">``./ficheiro.txt``</li>
  </ul>
<li class="fragment"> Frequent convention examples</li>
  <ul>
  <li class="fragment"><font color="red">~</font> -> *home dir*, ex. ``/home/francisco``</li>
  <li class="fragment"><font color="red">./</font> -> "Here"</li>
  <li class="fragment"><font color="red">../</font> -> One directory below</li>
  <li class="fragment"><font color="red">../../</font> -> Two 'dirs' below</li>
  <li class="fragment"><font color="red">.file</font> -> "Hidden" file</li>
  </ul>
</ul>

|||

### ["The PATH"](https://www.youtube.com/watch?v=b0wfu3tOrtQ)

```bash
cd

ls /etc/fstab

ls ~/

touch ./aa

cd Downloads

ls ../

touch .aa

ls

ls -a

```

---

### Navigation part II

1. How do I create a new dir (AKA - folder)? <!-- .element: class="fragment" data-fragment-index="1" -->
2. How do I erase a dir? <!-- .element: class="fragment" data-fragment-index="2" -->
3. How do I create a new file? <!-- .element: class="fragment" data-fragment-index="3" -->
4. How do I copy a file? <!-- .element: class="fragment" data-fragment-index="4" -->
5. How do I move a file? <!-- .element: class="fragment" data-fragment-index="5" -->
6. How do I erase a file? <!-- .element: class="fragment" data-fragment-index="6" -->

<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

|||

### Navigation part II


```bash
mkdir dir_name
rmdir dir_name  # Dir needs to be empty!
touch file_name
cp origin destination
mv origin destination
rm file_name
```

<p class="fragment" style="color:red;">Be **very**, **very** careful with `rm`</p>

---

## Take 5'

---

## Permissions

#### Every file has "attributes"

```bash 
ls -l
drwxr-xr-x  5 francisco francisco 4096 Oct 22 00:24 Desktop
drwxr-xr-x  3 francisco cobig2    4096 2013-05-20 13:55 Databases
-rw-r--r--  1 francisco francisco 4256 Sep 15  2011 Zkill.py
```
<ul>
<li class="fragment">3 scopes</li>
  <ul>
  <li class="fragment">user</li>
  <li class="fragment">group</li>
  <li class="fragment">others</li>
  </ul>
<li class="fragment">Each of which has 3 "values"</li>
  <ul>
  <li class="fragment"><font color="red">r</font>ead (4)</li>
  <li class="fragment"><font color="red">w</font>rite (2)</li>
  <li class="fragment">e<font color="red">x</font>ecute (1)</li>
  </ul>
</ul>

---

### Permissions Part II

<ul>
<li class="fragment">Changing ownership is simple:</li>
  <ul>
  <li class="fragment">`$ chown user:group file`</li>
  </ul>
<li class="fragment">Changing permissions... not so much:</li>
  <ul>
  <li class="fragment">`chmod mode file`</li>
    <ul>
    <li class="fragment">5 - read&execute (4+1)</li>
    <li class="fragment">6 - read&write (4+2)</li>
    <li class="fragment">7 - read&write&execute (4+2+1)</li>
    </ul>
  <li class="fragment">Using trios of <font color="green">U</font><font color="blue">G</font><font color="red">O</font> (<font color="green">U</font>ser<font color="blue">G</font>roup<font color="red">O</font>thers)</li>
    <ul>
    <li class="fragment">`777 or 755 or 640`</li>
    </ul>
  <li class="fragment">Or using a "verbal" form</li>
    <ul>
    <li class="fragment">`chmod +x file`</li>
    <li class="fragment">`chmod g-w file`</li>
    </ul>
  </ul>
</ul>

|||

### Permissions Part II

Try it:

```bash
groups  # Shows which groups your user belongs to

touch perm_test_file

# chmod and chown at will

```

---

### Environmental variables

Dynamically named values that can be "recalled"

```bash
test_var="hello"  # Defines the variable $test_var
echo $test_var  # What does `echo` do?
echo $USER  # Pre-defined variables
echo $HOME

# aliases are very similar:

alias ll="ls -l"
ll
```

<p class="fragment">Any env var or alias you set will be "forgotten" the moment you close your shell session.</p>

---

### Unix pipe `|`

#### Command output can be used as input for a following program

```bash
du -sm *  # What does `du` do?

du -sm *| sort -rk 2  # What does `sort` do?

du -sm *| sort -n
```

![Unlimited power](C03_assets/unlimited.gif)

---

### $PATH

<ul>
<li class="fragment">In order to run commands in a \*NIX environment you need to provide a full path to the program</li>
  <ul>
  <li class="fragment">`~/cool_stuff/my_program`</li>
  <li class="fragment">`cd ~/cool_stuff; ./my_program`</li>
  </ul>
<li class="fragment">Wait, what?</li>
<li class="fragment">Unless the programs are in a dir exposed to the $PATH variable</li>
  <ul>
  <li class="fragment">`/bin`</li>
  <li class="fragment">`/usr/bin`</li>
  <li class="fragment">`/usr/local/bin`</li>
  </ul>
</ul>

|||

### $PATH

```bash
echo $PATH  # View the contents of the $PATH var
mkdir ~/bin 

PATH=$PATH:~/bin

echo $PATH
```

---

### Arguments & whitespace

<ul>
<li class="fragment">A program and it's arguments are always split by a "whitespace"</li>
  <ul>
  <li class="fragment">`ls -l`</li>
  </ul>
<li class="fragment">Frequently, more than one argument is "passed" to the program</li>
  <ul>
  <li class="fragment">`cp origin destination`</li>
  </ul>
<li class="fragment">What if there is whitespace in a filename?</li>
  <ul>
  <li class="fragment">`filename with spaces.txt`</li>
  <li class="fragment">`ls filename\ with\ spaces.txt`</li>
  <li class="fragment">`ls 'filename with spaces.txt'`</li>
  </ul>
</ul>

---

### Awesome starts here

#### ``wget/curl grep sed cat less vim nano git``

```bash
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C03_assets/example.fasta  # See also `curl`

cat example.fasta

grep ">" example.fasta

sed 's/>/+/g' example.fasta

less example.fasta  # Press 'q' to exit less and return to the shell prompt

nano example.fasta
```

<p class="fragment">[Only the brave will be able to use Vim](https://vim-adventures.com/)</p>

<p class="fragment">We will return to `git` [another day](https://www.youtube.com/watch?v=iMfgcA1zlGY) </p>

---

### *Wildcards*

<ul>
<li class="fragment">Special characters that mean something other than their "literal"</li>
  <ul>
  <li class="fragment">"\*" - any character any number of times</li>
  <li class="fragment">"?" - any character, once</li>
  </ul>
</ul>

|||

### *Wildcards*

```bash
ls D*

cp *.txt some/other/dir
```

---

### *Redirects*

<ul>
<li class="fragment">Represented by the `>` character</li>
<li class="fragment">Very similar to `|`, but 'dumps' a program's output into a file instead of another program</li>
<li class="fragment">Can be used in may ways:</li>
  <ul>
  <li class="fragment">`ls -l > file.txt  # resets a file and writes to it`</li>
  <li class="fragment">`ls -l >> file.txt  # appends text to the end of the file`</li>
  </ul>
</ul>

---

### Combos

```bash
cat example.fasta | grep ">"  > sequence_names.txt

# Even better:

cat example.fasta |grep ">" | sed 's/>//g' > sequence_names_2.txt

# Who can figure this one out?
```

---

### How do redirects work?

<ul>
<li class="fragment">`stdin`</li>
<li class="fragment">`stdout`</li>
<li class="fragment">`stderr`</li>
  <ul>
  <li class="fragment">`0<` or `<`</li>
  <li class="fragment">`>1` or `>`</li>
  <li class="fragment">`>2`</li>
  </ul>
<li class="fragment">Be **very** careful when using ">" in shell commands</li>
</ul>

|||

### How do redirects work?

```bash
cat < example.fasta
cat < example.fasta > example_copy.fasta

python example.fasta > nope.txt
python example.fasta 2> yup.txt

# Check the contents of these 2 files and tell me what happened

```

---

### (De)compressing files

<ul>
<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>
<li class="fragment">Zip files</li>
  <ul>
  <li class="fragment">`unzip file.zip`</li>
  </ul>
<li class="fragment">"tar" files</li>
  <ul>
  <li class="fragment">`tar xvfz file.tar.gz`</li>
  <li class="fragment">`tar xvfj file.tar.bz2`</li>
  <li class="fragment">`tar xvfJ file.tar.xz`</li>
  </ul>
<li class="fragment">We can also use `tar` to compress</li>
  <ul>
  <li class="fragment">`tar cvfJ file.tar.xz dir_to_compress`</li>
  </ul>
<li class="fragment">We can use `gzip` to compress directly from `stdin`</li>
  <ul>
  <li class="fragment">`cat file.txt | gzip > file.txt.gz`</li>
  <li class="fragment">(Use `gunzip` to uncompress)</li>
  </ul>
</ul>

|||

### Try this:

```bash
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C03_assets/file.tar.gz
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C03_assets/file.tar.bz2
wget https://gitlab.com/StuntsPT/asb_2019/raw/master/docs/classes/C03_assets/file.tar.xz

# Uncompress all these files

# Concatenate the contents of all resulting files in a single file and compress it

```

---

### References

* [What is the linux shell](https://bash.cyberciti.biz/guide/What_is_Linux_Shell)
* [The $PATH variable](https://www.linux.com/answers/what-purpose-path-variable)
* [File permissions in Linux](https://www.linux.com/learn/understanding-linux-file-permissions)
* [Stdin and Stdout](http://www.learnlinux.org.za/courses/build/shell-scripting/ch01s04.html)
* [`tar` manpage](https://linux.die.net/man/1/tar)
