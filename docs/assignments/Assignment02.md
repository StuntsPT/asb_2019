# Assignment 02

## *High Throughput Sequencing at your fingertips*


### Deadline:

June 24th 2019


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### The data:

For this assignment you will be provided with [a pre-built High Throughput Sequencing (HTS) dataset](https://gitlab.com/StuntsPT/small_test_dataset/tree/master/Nyssa).
Along with the data you will find a small `.txt` file with important information regarding the data.


### Your Task:

* Figure out if the data has the resolution to distinguish the "CF-TX" (*Cornus florida*) group from the reminder taxa.
* Are there any other groups that you can identify?
* Write a report on what you did to try to answer that question;
* Keep in mind that your are not analysing the full dataset, but rather a small subset of the total data.


### In detail:

* Write an *introduction* section describing the species you are working on;
  * Highlight the importance of HTS data on being able to perform task you are resolving;
* Make sure you have a *Materials & Methods* section where you **detail** how your analyses were performed;
  * Do not forget to describe the dataset as best you can!
* Include a *results* section where you describe the results you have obtained;
* Interpret the results in a biological context in the *Discussion* section;
* Optionally, finish with a *conclusion* section if you think it makes sense in your specific case;


### Hints:

* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use, if you think it is adequate/relevant;
* This dataset might take up to 1h to analyse. Plan in advance;
* DO NOT FORGET TO INCLUDE REFERENCES!!
* The procedure to analyse this dataset *is simpler* than the one you have done in class;
* Do not hesitate to ask any questions you may have via email;

### Notes:

* Original data can be found in the [dryad repository](https://doi.org/10.5061/dryad.bb3h52t)
* Original research paper: [Resolving relationships and phylogeographic history of the Nyssa sylvatica complex using data from RAD-seq and species distribution modeling](https://doi.org/10.1016/j.ympev.2018.04.001)
