# Assignment 01

## *Knee deep into phylogenetics*


### Deadline:

~~May 6th 2019~~ May 13th 2019


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### Your task:


For this assignment you will have to:

* Search the bibliography for a research paper that performs phylogenetic analyses;
* Understand the proposed biological problem;
* Obtain the sequences used in that research paper;
* Reproduce (or improve) the analyses therein performed (you do not have to use all the methos present in the original paper, only those you deem more important);
* Interpret the obtained phylogenetic trees;
* Compare your results to those of the original paper;
* Write a report on what you did;


### In detail:

* Write a small *introduction* section describing the original biological problem present in the selected paper;
* Make sure you have a *Materials & Methods* section where you **detail** how your analyses were performed;
* Include any phylogenetic trees you deem necessary in the *results* section (and describe their most important features);
* Interpret the trees in a biological context in the *Discussion* section
* Optionally, finish with a *conclusion* section if you think it makes sense in your specific case


### Hints:

* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use, if you think it is adequate/relevant;
* Do not hesitate to ask any questions you may have via email;
